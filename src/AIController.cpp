#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"
#include "LuaBind/object.hpp"
#include "LuaScriptManager.h"

using namespace Ogre;


AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mCurrentParam(0.0f),
	mPath(patrol)
{
	root = scripting::Manager::getSingleton().callFunction<luabind::object>("init", this);

	// create a overlay to render the behavior tree
	DebugOverlay::getSingleton().addTextBox("ai" + spacecraft->getId(), "", 0, 100 + (150 * (spacecraft->getId()-1)), 300, 150);
}

void AIController::update(float delta)
{
	if (root)
	{
		root["tick"](root, delta);

		// "renders" the behavior tree on the overlay
		std::string text = luabind::object_cast<std::string>(root["printTree"](root, ""));
		DebugOverlay::getSingleton().setText("ai" + getSpacecraft()->getId(), text);
	}
}


 
